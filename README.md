# Infra

## Подготовка инфраструктуры
Для подготовки инфраструктуры выполните следующие команды:
   ```shell
   chmod +x ./Infra.sh #(только перед первым запуском)
   ./Infra.sh -a "create" -s "<path to private ssh key file>" -p "<your project id>" -c "<path to credentials file>" -r "<allowed source ip range>"
   ```

   или передите в папку terraform и выполните команды (система запросит переменные):
   ```shell
   terraform init -backend-config "<path to credentials file>" -migrate-state
   terraform apply --auto-approve 
   ```

## Удаление инфраструктуры
Для удаления инфраструктуры выполните следующую команду:
   ```shell
   chmod +x ./Infra.sh #(только перед первым запуском)
   ./Infra.sh -a "destroy" -s "<path to private ssh key file>" -p "<your project id>" -c "<path to credentials file>" -r "<allowed source ip range>"
   ```
   
   или передите в папку terraform и выполните команду (система запросит переменные):
   ```shell
   terraform destroy --auto-approve 
   ```