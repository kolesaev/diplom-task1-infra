# Назначаем bucket для tfstate
terraform {
  backend "gcs" {
    bucket  = "diplom1-tfstate"
    prefix  = "tfstate"
    region  = "europe-north1"
  }
}

# Создаём переменные
variable "project-id" {
  type = string
}
variable "credentials-path" {
  type = string
}
variable "ssh-keys-path" {
  type = string
}
variable "source-ranges" {
  type = string
}

# подключаем написанный модуль из дочерней папки terraform-infra
module "terraform" {
  source = "./terraform"
  # пробрасываем переменные в модуль
  project-id = var.project-id
  credentials-path = var.credentials-path
  ssh-keys-path = var.ssh-keys-path
  source-ranges = var.source-ranges
}

# Выводим в консоль ip балансера и домена 
output "balancer_and_domain_ip" {
  value = module.terraform.balancer_and_domain_ip
}

# Выводим в консоль ip инстанса 1
output "instance_ext_ip" {
  value = module.terraform.instance_ext_ip
}
