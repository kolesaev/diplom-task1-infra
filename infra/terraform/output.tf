
# Выводим в консоль ip балансера и домена 
output "balancer_and_domain_ip" {
  value = google_compute_global_address.default.address
}

# Выводим в консоль ip инстанса 1
output "instance_ext_ip" {
  value = data.google_compute_instance.first.network_interface.0.access_config.0.nat_ip
}
