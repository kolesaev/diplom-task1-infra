
# Создвём сеть
resource "google_compute_network" "default" {
  name                    = "network-diplom1"
  auto_create_subnetworks = false
}

# Создаём подсеть для наших инстансов
resource "google_compute_subnetwork" "default" {
  name          = "subnet-diplom1"
  ip_cidr_range = "10.0.1.0/24"
  network       = google_compute_network.default.id
}

# Создаём внешний ip адрес
resource "google_compute_global_address" "default" {
  name = "static-ip-diplom1"
}

# Прописываем IP-адрес балансера нашему домену
resource "google_dns_record_set" "main_domain_ip" {
  depends_on = [data.google_compute_instance.first]
  name         = "test.kolesaevdevopst1.tk."
  managed_zone  = "zone1"
  type         = "A"
  ttl          = 300
  rrdatas      = [google_compute_global_address.default.address]

# копируем файлы ansible
  provisioner "file" {
    source      = "ansible/"
    destination = "/home/user"
    # указываем способ подключения
    connection {
      type        = "ssh"
      user        = "user"
      private_key = "${file("${var.ssh-keys-path}")}"
      host        = "${data.google_compute_instance.first.network_interface.0.access_config.0.nat_ip}"
    }
  }
  
# устанавливаем ansible и обновляем порт ssh
  provisioner "remote-exec" {
    inline = [
      "sudo apt update && sudo apt install ansible -y",
      "ansible-playbook ~/playbook.yaml"
    ]
    # указываем способ подключения
    connection {
      type        = "ssh"
      user        = "user"
      private_key = "${file("${var.ssh-keys-path}")}"
      host        = "${data.google_compute_instance.first.network_interface.0.access_config.0.nat_ip}"
    }
  }
}
