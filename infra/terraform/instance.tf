
# Назначаем шаблон для инстансов
resource "google_compute_instance_template" "default" {
  name         = "mig-template-diplom1"
  machine_type = "e2-small"
  tags         = ["allow-health-check"]

  lifecycle {
    create_before_destroy = true
  }
  network_interface {
    network    = google_compute_network.default.id
    subnetwork = google_compute_subnetwork.default.id
    access_config {
      # этот блок создаёт внешний ip
    }
  }
  # назначаем образ установки ubuntu 20.04 minimal
  disk {
    source_image = "ubuntu-os-cloud/ubuntu-minimal-2004-lts"
    auto_delete  = true
    boot         = true
  }
  # назначаем ssh-ключи
  metadata = {
    ssh-keys = "user:${file("${var.ssh-keys-path}.pub")}"
  }
}

# Создаём менеджер группы инстансов
resource "google_compute_instance_group_manager" "default" {
  name     = "mig-diplom1"
  named_port {
    name = "http"
    port = 8080
  }
  version {
    instance_template = google_compute_instance_template.default.id
    name              = "primary"
  }
  base_instance_name = "vm"
  target_size        = 1
}

# Ждём появление инстансов, иначе, они не передадутся в ссылочный параметр группы инстансов
resource "time_sleep" "wait_40_seconds" {
  depends_on = [google_compute_instance_group_manager.default]
  create_duration = "40s"
}

# Создаём ссылку на группу инстрансов
data "google_compute_instance_group" "default" {
  depends_on = [time_sleep.wait_40_seconds]
  self_link = "${google_compute_instance_group_manager.default.instance_group}"
}

# Создаём ссылки на инстрансы
data "google_compute_instance" "first" {
  self_link =  tolist(data.google_compute_instance_group.default.instances)[0]
}
