
# Разрешаем трафик к нашим инстансам (пока так, так как в задании не указано какое-то ограничение)
resource "google_compute_firewall" "default" {
  name          = "fw-diplom1"
  direction     = "INGRESS"
  network       = google_compute_network.default.id
  source_ranges = ["${var.source-ranges}"]
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22", "8080"]
  }
  target_tags = ["allow-health-check"]
}
