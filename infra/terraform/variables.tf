
# Создаём переменные
variable "project-id" {
  type = string
}
variable "credentials-path" {
  type = string
}
variable "ssh-keys-path" {
  type = string
}
variable "source-ranges" {
  type = string
}
