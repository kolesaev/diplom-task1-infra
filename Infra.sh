#!/bin/bash
cd "$(dirname "$(realpath "$0")")/infra"

while getopts ":p:c:s:a:r:" flag;
do
    case "${flag}" in
        c) cred=${OPTARG};;
        p) id=${OPTARG};;
        s) ssh=${OPTARG};;
        a) action=${OPTARG};;
        r) sranges=${OPTARG};;
        *) x=${OPTARG};;
    esac
done;

# указываем файл с правами для terraform backend
terraform init -backend-config "credentials=$cred" -migrate-state;

if [[ $action == "create" ]] ; then
    terraform apply --auto-approve -var "project-id=$id" -var "credentials-path=$cred" -var "ssh-keys-path=$ssh" -var "source-ranges=$sranges"
fi;

if [[ $action == "destroy" ]] ; then
    terraform destroy --auto-approve -var "project-id=$id" -var "credentials-path=$cred" -var "ssh-keys-path=$ssh" -var "source-ranges=$sranges"
fi;
